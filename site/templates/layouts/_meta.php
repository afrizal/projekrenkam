<meta charset="utf-8">
<meta name="description" content="Join millions and bring your ideas and projects to life with Envato - the world's leading marketplace and community for creative assets and creative people.">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0, width=device-width">
<meta name="google-site-verification" content="DXhEM19Ds0tzcJPqHqCp1uoRQJd0PUCKQkDo3RABkUA">
<!-- Favicons-->
<link rel="apple-touch-icon" sizes="57x57" href="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/favicon-194x194.png" sizes="194x194">
<link rel="icon" type="image/png" href="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/manifest.json">
<link rel="mask-icon" href="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/safari-pinned-tab.svg" color="#82b541">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/mstile-144x144.png">
<meta name="theme-color" content="#ffffff">
<!-- OpenGraph Meta-->
<meta property="og:image" content="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/og-image.jpg">
<!-- Image must be between 200x200 to 1500x1500px-->
<meta property="og:title" content="Envato - Top digital assets &amp; services">
<meta property="og:description" content="Join millions &amp; bring your ideas and projects to life with Envato - the world's leading marketplace and community for creative assets and creative people.">
<meta property="og:site_name" content="Envato">
<meta property="og:type" content="website">
<!-- Twitter Card Meta-->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@envato">
<meta name="twitter:title" content="Envato - Top digital assets &amp; services">
<meta name="twitter:description" content="Join millions &amp; bring your ideas and projects to life with the world's leading marketplace and community for creative assets and creative people.">
<meta name="twitter:image" content="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/images/favicons/twitter-image.jpg">
<!-- Remove iOS Chrome from Home Screen Bookmark-->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
