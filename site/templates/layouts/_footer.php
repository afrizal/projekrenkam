
<div class="final_section">
  <div class="inner_wrapper">
    <h4>Intelligent, smart & always with you.</h4>
    <p>The location app thats all about you, there when you need it most.</p>
    <a href="#" class="appstore_button">Available on the App Store</a>
    <a href="https://twitter.com/intent/tweet?text=Check%20Out%20App%20Name&url=http://jamiepeak.co.uk/" class="tweet_button">Tweet</a>
    <a href="https://www.facebook.com/" class="fb_button">Like</a>
  </div>
</div>

</div>

<div class="footer">
  <div class="inner_wrapper">
    <span class="right_reserved">All rights reserved.</span>© 2014 App Name <a href="#">Press Kit</a>
  </div>
</div>
