<script src="https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/js/modernizr-2.8.3.min.js"></script>
<script>
	// loadJS: load a JS file asynchronously.
	// Documentation: https://github.com/filamentgroup/loadJS
	function loadJS(e, t) {
		'use strict';
		var n = window.document.getElementsByTagName('script')[0],
			o = window.document.createElement('script');
		return o.src = e, o.async = !0, n.parentNode.insertBefore(o, n), t && 'function' == typeof t && (o.onload = t), o
	}
	loadJS('https://18482-presscdn-0-9-pagely.netdna-ssl.com/wp-content/themes/envato/assets/js/app.compiled.js');
	// Google analytics
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o), m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
	ga('create', 'UA-11834194-1', 'auto');
	ga('send', 'pageview');
</script>
